## What to change

### General

- Opening braces should go on the same line
- Prefer single quotes to double quotes

### HTML

- Separate substantial scripts and styles from markup/down
- Recommend separating chunks of related code by blank lines
- Recommend separating block level elements by newline
- Discourage breaking attributes by-line unless it improves readability

### CSS

- Allow single properties to go on one line under multiple lines of selectors
- Allow multiple short selectors to appear on one line
- Recommend, but not enforce, property order

### JavaScript

- Place ||, &&, +, etc on the next line
