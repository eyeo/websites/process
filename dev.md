# Development processes

## Website change process

1. Create an issue to propose a change
2. Submit a merge request to propose a solution
3. Merge a merge request to resolve an issue

## 1. Create an issue to propose a change

An issues's scope should be limited by what can be completed by its resolver in an iteration.

A change's scope should be limited to what is *necessary* to fix an existing problem or implement a new feature.

In most cases, an issue should propose a single change. However, an issue may propose multiple related small changes. And a complicated change may be referred to by or resolve multiple issues.

## 2. Submit a merge request to propose a solution

In most cases, a merge request should propose a solution to resolve a single issue completely. However, a merge request may resolve multiple related issues. And a complicated issue may be referred to or resolved by multiple merge requests. 

The following things should be considered, in order of priority, when assesing a merge request:

1. Does the change do what it says it does?
2. Does the change meet its specification?
3. Is the solution proposed appropriate for the issue?
   - e.g. Does it do more or less than necessary?
   - e.g. Does it create other issues?
4. Does the solution proposed meet our code quality and style standards?

Minor code quality and style issues should not block merging when the merge request fixes a problem or implements a feature that immediately benefits our users.

Detailed cross-browser testing and design review should not be performed in merge request review unless the issue specifies.

## 3. Merging a merge request to resolve an issue

A merge request should produce a single commit that refers to and resolves issues appropriately when merged. e.g.

```
// Resolve a typo
Fixed #NN - Fixed typo on this section of that page

// Impact a feature
Refd #NN - Implemented a standalone part of an epic feature

// Fix an issue that resolves and impacts several others
Fixed #NN, #NN, Refd #NN, #NN - Changed the way this component of that layout functions
```

If the merge request is out-of-date, or produces conflicts, then the merge request author should resolve them and the merge request reviewer should confirm that they were resolved properly.

If QA is not requested by the issue referred to, or another relevant issue, and the change impacts the design or functionality of the website in a non-trival way, then the merge request reviewer should request QA appropriately before or after merging.

A change that fixes as issue by commit hook should not be merged before testing if testing is necessary.
