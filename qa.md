# QA processes

## From development

A change may be passed from development to QA via:

1. QA issue
   - Added to an iteration appropriately
2. Label
   - When an issue's scope includes testing

Developers may perscribe QA via label when the issue's scope did not previously include testing at their discretion, and testers may choose to accept their perscriptions within the scope of the issue in an iteration, or request a separate issue to be added to the backlog appropriately.
