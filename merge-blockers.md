# Merge request blockers

This is a categorized list of issues that may block merge requests from being merged.

> Note: Not all issues that *may* block merge requests from being merged *should* block merge requests from being merged. Reviewers may fix small issues themselves and preface issue report comments with NIT (Nit-picking) to indicate that an issue is not blocking in their opinion.

- Title
    - **Ref**: Does not refer to a relevant issue
    - **Fix**: Does not fix a relevant issue
    - **Description**: Does not describe the change made precisely
    - **Tense**: Does not describe the change in the past tense
    - **WIP**: Failed to update **W**ork **I**n **P**rogress flag
- Description
    - **Build**: Does not contain necessary build instructions
    - **Test**: Does not contain necessary testing instructions
- Changes
    - **Out-of-scope**: Not related to the issue(s) referred to
    - **Unnecessary**: Not necessary to resolve the issue(s) referred to
    - **Functional**: Does not work as intended
    - **Incomplete**: Does not completely resolve the issue(s)
    - **Breaking**: Unintentionally breaks something
    - **Suboptimal**: Can be significantly improved without broadening scope
    - **Design**: Design is obviously not right yet
    - **Report**: Failed to report known issue
    - **Comment**: Failed to comment nuance code
    - **Style**: Code style linting error
- Commit
  - **Title**: See "Title" blockers above
  - **Change**: Implements more or less than one change
  - **Conflict**: Creates or imports unnecessary merge conflict artifacts
- Review
  - **Round**: Update introduces issues
